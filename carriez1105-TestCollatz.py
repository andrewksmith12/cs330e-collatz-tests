#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "1 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 1)
        # making sure same number does not affect reading

    def test_read_3(self):
        s = "999999 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  999999)
        self.assertEqual(j, 1)
        # making sure order of and upper/lower limit numbers do not affect reading
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)
    # checking for numbers within the same range of 1000

    def test_eval_2(self):
        v = collatz_eval(1000, 2000)
        self.assertEqual(v, 182)
    # checking caching mechanism to catch edges of intervals

    def test_eval_3(self):
        v = collatz_eval(702, 53271)
        self.assertEqual(v, 340)
    # checking caching mechanism; see if large gaps between numbers are a problem

    def test_eval_4(self):
        v = collatz_eval(999997, 999999)
        self.assertEqual(v, 259)
    # added another test for checking the upper range of valid input values

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
        # default unit test

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 1, 2, 3)
        self.assertEqual(w.getvalue(), "1 2 3\n")
        # making sure it prints regardless of accuracy of values

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 3, 3, 8)
        self.assertEqual(w.getvalue(), "3 3 8\n")
        # making sure bringing up same value doesn't cause any problems

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
        # default unit test

    def test_solve_2(self):
        r = StringIO("1 1\n100 101\n201 200\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "1 1 1\n100 101 26\n201 200 27\n")
        # testing intervals between r and w of 0, 1, -1, and fewer string items

    def test_solve_3(self):
        r = StringIO("999997 999999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "999997 999999 259\n")
        # testing upper edge case, single input from string

    def test_solve_4(self):
        r = StringIO("")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "")
        # testing empty string input

    def test_solve_5(self):
        r = StringIO("1 1000\n702 53271\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "1 1000 179\n702 53271 340\n")
        # testing larger intervals, edges of cache intervals
# ----
# main
# ----

if __name__ == "__main__": #pragma: no cover
    main() #pragma: no cover



""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
